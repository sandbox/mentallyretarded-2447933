Menu Settings
-------------
Theme allows two styles, One , regular menu style, for that, you need to enable
main menu in theme settings page. Otherwise theme will display a button to
toggle the display of black widget containing About text and social icons.

About Text And Social Icons
---------------------------
Go to theme settings page where configuration options for social icons and
About text can be found in Summary Theme Settings fieldset.
